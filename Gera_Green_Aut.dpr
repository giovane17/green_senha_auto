program Gera_Green_Aut;

uses
  Vcl.Forms,
  frmSenha in 'frmSenha.pas' {Frm_Green},
  U_Funcoes in '..\U_Funcoes.pas',
  dm_dados in 'dm_dados.pas' {DmDados: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrm_Green, Frm_Green);
  Application.CreateForm(TDmDados, DmDados);
  Application.Run;
end.
