unit frmSenha;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Data.DB,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids, System.Zip, IniFiles, midaslib,
  Vcl.ComCtrls, RpCon, RpConDS, RpBase, RpSystem, RpDefine, RpRave;

type
  TFrm_Green = class(TForm)
    Bt_senha: TBitBtn;
    Cds1: TClientDataSet;
    Cds1CARTAO: TStringField;
    Cds1NOME: TStringField;
    Cds1SENHA: TStringField;
    Cds1EMPRESA: TStringField;
    Cds1MATRICULA: TStringField;
    Cds1DEPARTA: TStringField;
    Cds1PRODUTO: TStringField;
    DataSource1: TDataSource;
    Cds1nomearq: TStringField;
    Cds1tipo_carta: TIntegerField;
    EdCartas: TEdit;
    Label1: TLabel;
    Button1: TButton;
    EdPedido: TEdit;
    Label2: TLabel;
    Cds1remis: TIntegerField;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    Button2: TButton;
    RvProject1: TRvProject;
    RvSystem1: TRvSystem;
    RvProtocolo: TRvDataSetConnection;
    RvClientes: TRvDataSetConnection;
    procedure Bt_senhaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Imprime_capa_lote(xlote : string);
    procedure Imprime_separador(xnome_separador : string);
    function ListarArquivos(Diretorio: string; Sub:Boolean):tstringlist;
    function TemAtributo(Attr, Val: Integer): Boolean;
    function qual_produto(xprod : string): string;
    function UnZip(ZipName: string; Destination: string): boolean;
    procedure imprime();
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_Green: TFrm_Green;
  list_arq : tstringlist;
  log,list_arquivos, arq : TStringList;
  I: Integer;
  G: Integer;
  produto: string;
  cont_arq: Integer;
  nomprn: string;
  prn,bat : TextFile;
  L,C,ycont, cont_1000: Integer;
  nomearq: string;
  L1: Integer;
  reemis : boolean;
  startupinfo: Tstartupinfo;
  ProcessInfo: TprocessInformation;

implementation

{$R *.dfm}

uses U_Funcoes, dm_dados;

procedure TFrm_Green.Bt_senhaClick(Sender: TObject);
var
  proximo_id: integer;
begin

  try
  try
   //Verifica se tem arquivo para processar

    with DmDados do begin
      FDTransaction1.StartTransaction;
      QueryArqEntrada.Close;
      QueryArqEntrada.SQL.Text := 'select sm.*, a.*,c.nome_fantasia,s.descricao from arqentradas a join clientes c on c.codigo = a.ARQ_COD_CLI '+
                                  'join servicos_clientes s on s.codigo = a.ARQ_COD_SERV '+
                                  'join servicos_mkt sm on sm.cod_inter = s.cod_serv_cobranca where a.ARQ_STATUS = 2 and a.ARQ_COD_CLI = 4 and a.ARQ_COD_SERV = 306';
      QueryArqEntrada.open;
      if QueryArqEntrada.RecordCount = 0 then begin
        log.Add(FormatDateTime('dd-mm-yyyy',date)+' Sem arquivos para processar.');
        abort;
      end else begin
        log.Add(FormatDateTime('dd-mm-yyyy',date)+' Tem '+IntToStr(QueryArqEntrada.RecordCount)+' arquivos para processar!');
      end;

      UnZip('E:\Processamento_automatico\'+QueryArqEntrada.FieldByName('ARQ_ARQUIVOZIP').AsString, ExtractFilePath(Application.ExeName)+'\arquivos\'+QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'\');
      ListarArquivos(ExtractFilePath(Application.ExeName)+'\arquivos\'+QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'\',false).Text;
    end;

    Cds1.Close;
    Cds1.CreateDataSet;
    Cds1.Open;

    for I := 0 to list_arq.Count-1 do begin
      arq.LoadFromFile(list_arq[I]);
      // la�o de importa��o
      for G := 3 to arq.Count-1 do begin
        Cds1.Append;
        Cds1CARTAO.AsString   := copy(arq[G],001,016);
        Cds1NOME.AsString     := copy(arq[G],020,029);
        Cds1SENHA.AsString    := copy(arq[G],049,005);
        Cds1EMPRESA.AsString  := trim(copy(arq[G],058,028));
        Cds1MATRICULA.AsString:= copy(arq[G],087,016);
        Cds1DEPARTA.AsString  := copy(arq[G],103,030);
        Cds1PRODUTO.AsString  := copy(arq[G],133,003);
        Cds1nomearq.AsString  := ExtractFileName(list_arq[I]);
        Cds1tipo_carta.AsString := copy(arq[0],56,5);
        Cds1.Post;
      end;
    end; // for arquivos

    Cds1.SaveToFile('E:\prog_auto\Green\Xml\'+DmDados.QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'.xml');

       cds1.First;

  // Impressao
  imprime();
  // Update na tabela arqentradas
  with DmDados do begin
    QueryConsulta.close;
    QueryConsulta.SQL.Text:='update arqentradas ar set ar.ARQ_STATUS = 3, ar.ARQ_TOTAL ='+IntToStr(ycont)+' where ar.id ='+QueryArqEntrada.FieldByName('id').AsString ;
    QueryConsulta.ExecSQL;
  end;
  //update na tabela arquivos
  with DmDados do begin
    Query_planilha.Close;
    Query_planilha.SQL.text := 'update arquivos ar set ar.codigo_situacao = 7 ,ar.quantidade_processada = '+IntToStr(ycont-1)+' where ar.codigo = '+QueryArqEntrada.FieldByName('arq_num_pedido').AsString;
    Query_planilha.ExecSQL;
  // Verifica qua o proximo ID
    Query_planilha.Close;
    Query_planilha.SQL.Text := 'SHOW TABLE STATUS LIKE '+quotedstr('planilha');
    Query_planilha.Open;
    proximo_id := Query_planilha.FieldByName('auto_increment').AsInteger;

  // Insert do protocolo
    Query_planilha.Close;
    Query_planilha.sql.Text :='INSERT INTO `planilha` (`cod_cli`, `quantidade`, `obs`, `sequencia`, `situacao`, `data`, `usuario`, `vlr_tmp`, '+
    '`dt_entrega`, `obs_entrega`, `id_prod`, `nota_num`, `num_pedido`, `motivo_cancel`, `data_impressao`) VALUES ('+
    QueryArqEntrada.FieldByName('cod_cli').AsString+','+IntToStr(ycont-1)+',null,null,1,'+QuotedStr(FormatDateTime('yyyy-mm-dd',date))+',26,'+
    QuotedStr(StringReplace(QueryArqEntrada.FieldByName('valor_uni').AsString,',','.',[rfReplaceAll,rfIgnoreCase]))+',null,null,'+QueryArqEntrada.FieldByName('cod_inter').AsString+',null,'+
    QueryArqEntrada.FieldByName('arq_num_pedido').AsString+',null,null)';
    Query_planilha.ExecSQL;

  // insere relacao protocolo x pedido
    Query_planilha.Close;
    Query_planilha.SQL.Text := 'INSERT INTO `protocoloxpedido` (`num_pedido`,`num_protocolo`) values ( '+QueryArqEntrada.FieldByName('arq_num_pedido').AsString+','+IntToStr(proximo_id)+')';
    Query_planilha.ExecSQL;

  // envia_email
    Query_planilha.Close;
    Query_planilha.sql.Text :='INSERT INTO `email_envio_automatico` (`assunto`, `email_destino`, `mensagem`, `situacao`) VALUES ( '+
    quotedstr('Green Card - Protocolo Numero: '+inttostr(proximo_id)+' do pedido numero: '+QueryArqEntrada.FieldByName('arq_num_pedido').AsString)+','+
    QuotedStr('dados@postalmkt.com.br')+','+QuotedStr('Processo automatico de Geracao do Cliente Green Card concluido')+',null)';
    Query_planilha.ExecSQL;

  // Imprime protocolo
    Query_planilha.Close;
    Query_planilha.SQL.Text := 'select p.id,p.situacao ,p.cod_cli, '+
    'p.dt_entrega,c.NOME_CLIENTE,s.produto,p.quantidade,p.vlr_tmp,(p.quantidade * s.valor_uni) as total,p.data,p.obs,p.motivo_cancel, '+
    'u.usuario,(select GROUP_CONCAT(pp.num_pedido) from protocoloxpedido pp where pp.num_protocolo = p.id) as num_pedido from planilha p '+
    'join CLIENTES_MKT c on c.CODCLIE = p.cod_cli join servicos_mkt s on s.cod_inter = p.id_prod join usuarios u on u.codigo = p.usuario where '+
    ' p.id = '+inttostr(proximo_id);
    Query_planilha.Open();

    RvProject1.SetParam('pprot',Query_planilha.FieldByName('num_pedido').AsString);
    RvProject1.ProjectFile :=ExtractFilePath(Application.ExeName)+'protocolo.rav';
    RvProject1.Execute;


  end;

  DmDados.FDTransaction1.Commit;
  close;

  except on e: Exception do begin
    log.Add(e.Message);
    DmDados.FDTransaction1.Rollback;
  end;
  end; // except
  finally
    log.SaveToFile(ExtractFilePath(Application.ExeName)+'\log\log'+FormatDateTime('dd-mm-yyyy-hhmmss',now)+'.txt');
    log.Free;
    arq.Free;
    list_arquivos.Free;
    list_arq.Free;
    application.Terminate;
  end;

end;

procedure TFrm_Green.Button1Click(Sender: TObject);
var
  xremis : TStringList;
begin
  Cds1.LoadFromFile('E:\prog_auto\Green\Xml\'+EdPedido.Text+'.xml');
  Cds1.Open;

  xremis := TStringList.Create;
  xremis.Text := StringReplace(EdCartas.Text,',',#13#10,[rfReplaceAll,rfIgnoreCase]);
  xremis.Sort;

  for I := 0 to Cds1.RecordCount-1 do begin
    if xremis.IndexOf(IntToStr(Cds1.RecNo)) = -1 then begin
      Cds1.Edit;
      Cds1.FieldByName('remis').AsString := '1';
      Cds1.Post;
    end;
    Cds1.Next;
  end;

  xremis.Free;
  Cds1.First;
  imprime;
  StatusBar1.SimpleText := 'Total -> '+IntToStr(Cds1.RecordCount);
  ShowMessage('Concluido');
end;

procedure TFrm_Green.Button2Click(Sender: TObject);
var
  xremis : TStringList;
begin
  Cds1.LoadFromFile('E:\prog_auto\Green\Xml\'+EdPedido.Text+'.xml');
  Cds1.Open;

{  xremis := TStringList.Create;
  xremis.Text := StringReplace(EdCartas.Text,',',#13#10,[rfReplaceAll,rfIgnoreCase]);
  xremis.Sort;

  for I := 0 to Cds1.RecordCount-1 do begin
    if xremis.IndexOf(IntToStr(Cds1.RecNo)) = -1 then begin
      Cds1.Edit;
      Cds1.FieldByName('remis').AsString := '1';
      Cds1.Post;
    end;
    Cds1.Next;
  end;

  xremis.Free;}
  Cds1.First;
  imprime;
  StatusBar1.SimpleText := 'Total -> '+IntToStr(Cds1.RecordCount);
  ShowMessage('Concluido');
end;

procedure TFrm_Green.FormActivate(Sender: TObject);
var
  ArquivoINI: TIniFile;
begin
  ArquivoINI := TIniFile.Create(ExtractFilePath(Application.ExeName)+'conf.ini');
  if  ArquivoINI.ReadString('CONFIG', 'MODO_AUTO','ERRO')='TRUE' then begin
    reemis := false;
    Bt_senha.Click;
  end else begin
    reemis := true;
    Bt_senha.Visible := false;
  end;

  ArquivoINI.Free;
end;

procedure TFrm_Green.FormCreate(Sender: TObject);
begin
  log := TStringList.Create;
  list_arquivos := TStringList.Create;
  arq := TStringList.Create;

end;

procedure TFrm_Green.imprime;
var
  als: string;
  list_arquivos : tstringlist;
  H: Integer;
begin
  cont_arq:=1;
  nomprn:= 'arq-' + Formatar(IntToStr(cont_arq),2,FALSE,'0') + '.PRN';
  AssignFile(Prn,nomprn);
  Rewrite(Prn);
  Write(Prn,'&l1S&l0o26A&l4d1E');

  AssignFile(bat,'copia.bat');
  Rewrite(bat);
  Writeln(bat, 'copy /b capa.prn+sep.prn+f*.prn macro.prn');
  log.Add(FormatDateTime('dd-mm-yyyy',date)+' Iniciando processamento...');


  list_arquivos := TStringList.Create;

  ycont := 1;
  nomearq :='';
  cont_1000:=0;

  if reemis = false then
    Imprime_capa_lote(IntToStr(cont_arq));

  nomearq := Cds1nomearq.AsString;

  while not(Cds1.Eof) do begin

  if trim(Cds1remis.AsString) = '' then begin

    Write(Prn,'&a1G');

    if (Cds1PRODUTO.AsString <> '407') and (Cds1PRODUTO.AsString <> '500') then
      Write(Prn,'&f1y2X&u300D');

    if (Cds1PRODUTO.AsString ='500') then // rota card
      Write(Prn,'&f10y2X&u300D');

    if (Cds1PRODUTO.AsString = '407') then // super conta
      Write(Prn,'&f11y2X&u300D');

    if (Cds1tipo_carta.AsString = '2') then // fortal com 0800 e sem RH
      Write(Prn,'&f12y2X&u300D'); //0800

    if (Cds1tipo_carta.AsString = '1') then // STV com 0800
      Write(Prn,'&f12y2X&u300D');  //0800

    if (Cds1tipo_carta.AsString <> '2') and ((Cds1PRODUTO.AsString <>'500') or (Cds1PRODUTO.AsString ='407')) then
      Write(Prn,'&f13y2X&u300D');  // RH

    if (Cds1tipo_carta.AsString = '3') then // Bloqueio inicial
      Write(Prn,'&f14y2X&u300D');

    if ((Cds1PRODUTO.AsString = '300') or (Cds1PRODUTO.AsString = '310')) and (Cds1tipo_carta.AsString <> '4') then begin
      Write(Prn,'&f2y2X&u300D');
    end else if (Cds1PRODUTO.AsString = '300') and (Cds1tipo_carta.AsString = '4') then begin
      Write(Prn,'&f3y2X&u300D');                         // rede fechada
    end else if ((Cds1PRODUTO.AsString = '301') or (Cds1PRODUTO.AsString = '311')) and (Cds1tipo_carta.AsString <> '4') then begin
      Write(Prn,'&f4y2X&u300D');
    end else if (Cds1PRODUTO.AsString = '301') and (Cds1tipo_carta.AsString = '4') then begin
      Write(Prn,'&f5y2X&u300D');                        // rede fechada
    end else if (Cds1PRODUTO.AsString = '406')  then begin
      Write(Prn,'&f6y2X&u300D');
    end else if (Cds1PRODUTO.AsString = '403')  then begin
      Write(Prn,'&f7y2X&u300D');
    end;

    L:=585;
    C:=750;
    write(Prn,'(9U(s1p11v0s3b16602T' );
    L1:= 67;
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'X' + Cds1NOME.AsString );
    L:=L+L1;
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C+205)+'X' + Cds1MATRICULA.AsString );
    L:=L+L1;
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C+290)+'X' + Cds1EMPRESA.AsString );
    L:=L+L1;
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C+205)+'X' + Cds1DEPARTA.AsString );
    write(Prn,'(9U(s1p7v0s0b16602T' );
    write(Prn,'*p'+inttostr(L+40)+'y'+inttostr(C+900)+'X' + IntToStr(Cds1.RecNo) );
    write(Prn,'*p'+inttostr(L-201)+'y'+inttostr(C+900)+'X' + qual_produto(Cds1PRODUTO.AsString) );
    write(Prn,'(9U(s1p11v0s3b16602T' );
    L:=L+1810;
    write(Prn,'*p'+inttostr(L-20)+'y'+inttostr(C-450)+'X' + Cds1NOME.AsString );
    L:=L+130;
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C-450)+'X' + Cds1SENHA.AsString );
    write(Prn,'(9U(s1p5v0s0b16602T' );
    Write(Prn,'&a90P');
    write(Prn,'*p20y2600X' + Cds1nomearq.AsString );
    Writeln(Prn,'&a0P');

    inc(ycont);
  end;  // if reemissao

    Cds1.Next;

        // SEPARADOR
    if (nomearq <> Cds1nomearq.AsString) and (reemis = false) then begin
      log.Add(FormatDateTime('dd-mm-yyyy',date)+' Processando Arquivo: '+Cds1nomearq.AsString);
      imprime_separador(nomearq);
      nomearq := Cds1nomearq.AsString;
    end;

    inc(cont_1000);

  if cont_1000 = 1000 then begin
    if reemis = true then begin
      Writeln(bat, 'copy /b macro.prn+'+nomprn+' GreenCard-'+EdPedido.Text+'-REM-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
      list_arquivos.Add('GreenCard-'+EdPedido.Text+'-REM-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
    end else begin
      Writeln(bat, 'copy /b macro.prn+'+nomprn+' GreenCard-'+DmDados.QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'-'+DmDados.QueryArqEntrada.FieldByName('ID').AsString+'-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
      list_arquivos.Add('GreenCard-'+DmDados.QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'-'+DmDados.QueryArqEntrada.FieldByName('ID').AsString+'-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
    end;

    CloseFile(prn);
    cont_1000 := 0;
    inc(cont_arq);
    nomprn:= 'arq-' + Formatar(IntToStr(cont_arq),2,FALSE,'0') + '.PRN';
    AssignFile(Prn,nomprn);
    Rewrite(Prn);
    Write(Prn,'&l1S&l0o26A&l4d1E');
    if reemis = false then
      Imprime_capa_lote(IntToStr(cont_arq));
  end;

  end;  // while impressao

  imprime_separador(nomearq);

  log.Add(FormatDateTime('dd-mm-yyyy',date)+' Finalizando processamento...');

  if reemis = true then begin
    Writeln(bat, 'copy /b macro.prn+'+nomprn+' GreenCard-'+EdPedido.Text+'-REM-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
    list_arquivos.Add('GreenCard-'+EdPedido.Text+'-REM-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
  end else begin
    Writeln(bat, 'copy /b macro.prn+'+nomprn+' GreenCard-'+DmDados.QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'-'+DmDados.QueryArqEntrada.FieldByName('ID').AsString+'-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
    list_arquivos.Add('GreenCard-'+DmDados.QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'-'+DmDados.QueryArqEntrada.FieldByName('ID').AsString+'-'+Formatar(IntToStr(cont_arq),4,false,'0')+'.prn');
  end;
  CloseFile(prn);
  CloseFile(bat);

   FillChar(startupinfo,sizeof(Tstartupinfo),0);
    startupinfo.cb:=sizeof(Tstartupinfo);
    als := ExtractFilePath(Application.ExeName);
      if createProcess(nil, PCHAR(als+'copia.bat'),nil,nil,false,SW_HIDE,nil,PCHAR(ALS),startupinfo,processinfo) then
    waitforsingleobject(processinfo.hprocess,infinite);
    closehandle(processinfo.hprocess);


  for H := 0 to list_arquivos.Count-1 do begin
    if CopyFile(pwidechar(ExtractFilePath(Application.ExeName)+list_arquivos[H]),pwidechar('E:\spool\'+list_arquivos[H]),false) = true then
      log.Add(FormatDateTime('dd-mm-yyyy',date)+' Arquivo copiado com, sucesso...')
    else begin
      log.Add(FormatDateTime('dd-mm-yyyy',date)+' Erro ao copiar o arquivo...');
    // envia_email
      DmDados.Query_planilha.Close;
      DmDados.Query_planilha.sql.Text :='INSERT INTO `email_envio_automatico` (`assunto`, `email_destino`, `mensagem`, `situacao`) VALUES ( '+
      quotedstr('Erro ao copiar o arquivo')+','+
      QuotedStr('giovane@postalmkt.com.br')+','+QuotedStr('Erro ao copiar o arquivo '+list_arquivos[H])+',null)';
      DmDados.Query_planilha.ExecSQL;
    end;

  end;



{    FillChar(startupinfo,sizeof(Tstartupinfo),0);
    startupinfo.cb:=sizeof(Tstartupinfo);
    als := ExtractFilePath(Application.ExeName);
      if createProcess(nil, PCHAR(als+'copia.bat'),nil,nil,false,SW_HIDE,nil,PCHAR(ALS),startupinfo,processinfo) then
    waitforsingleobject(processinfo.hprocess,infinite);
    closehandle(processinfo.hprocess);
}
//  WinExec(pansichar(ExtractFilePath(Application.ExeName)+'\copia.bat'),SW_HIDE);

end;

procedure TFrm_Green.Imprime_capa_lote(xlote : string);
begin

      Write(Prn,'&a1G');
      L:=800;
      C:=300;
      write(Prn,'(9U(s1p25v0s3b16602T' );
      write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'XCLIENTE: '+DmDados.QueryArqEntrada.FieldByName('nome_fantasia').AsString  );
      l:=l+510;
      write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'XPRODUTO: '+DmDados.QueryArqEntrada.FieldByName('descricao').AsString  );
      l:=l+510;
      write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'XPEDIDO:  '+DmDados.QueryArqEntrada.FieldByName('ARQ_NUM_PEDIDO').AsString+'       MOV:  '+DmDados.QueryArqEntrada.FieldByName('ID').AsString   );
      l:=l+510;
      write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'XLOTE:  '+ Formatar(xlote,4,false,'0')  );
      l:=l+510;
      write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'XQUANTIDADE:  '+IntToStr(Cds1.RecordCount)  );
      Write(Prn,'&f98y2X&u300D');

end;

procedure TFrm_Green.Imprime_separador(xnome_separador : string);
begin
    Write(Prn,'&a1G');
    L:=300;
    C:=300;
    write(Prn,'(9U(s1p12v0s3b16602T' );
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C+700)+'XSEPARADOR'  );
    l:=l+200;
    write(Prn,'*p'+inttostr(L)+'y'+inttostr(C)+'X' + xnome_separador );
    Write(Prn,'&f99y2X&u300D');
end;

function TFrm_Green.ListarArquivos(Diretorio: string; Sub: Boolean) : tstringlist;
var
  F: TSearchRec;
  Ret: Integer;
  TempNome: string;
begin

  Ret := FindFirst(Diretorio+'\*.*', faAnyFile, F);
  list_arq:= TStringList.Create;

  try
    while Ret = 0 do
    begin
      if TemAtributo(F.Attr, faDirectory) then
      begin
        if (F.Name <> '.') And (F.Name <> '..') then
          if Sub = True then
          begin
            TempNome := Diretorio+'\' + F.Name;
            ListarArquivos(TempNome, True);
          end;
      end
      else
      begin
        list_arq.Add(Diretorio+'\'+F.Name);
      end;
        Ret := FindNext(F);
    end;
  finally
  begin
    FindClose(F);
  end;
end;

result:= list_arq;

end;



function TFrm_Green.qual_produto(xprod: string): string;
begin
   if xprod = '300' then
     result := 'Alimenta��o'
   else
   if xprod = '301' then
     result := 'Refei��o'
   else
   if xprod = '403' then
     result := 'Cultura'
   else
   if xprod = '406' then
     result := 'Combust�vel'
   else
   if xprod = '310' then
     result := 'Chip Alimenta��o'
   else
   if xprod = '311' then
     result := 'Chip Refei��o'
   else
   if xprod = '413' then
     result := 'Chip Cultura'
   else
   if xprod = '416' then
     result := 'Chip Combust�vel'
   else
   if xprod = '500' then
     result := 'Rota card'
   else
   if xprod = '407' then
     result := 'Super Conta'
   else
     result := 'Produto desconhecido';


end;

function TFrm_Green.TemAtributo(Attr, Val: Integer): Boolean;
begin
  Result := Attr and Val = Val;
end;

function TFrm_Green.UnZip(ZipName, Destination: string): boolean;
var
  UnZipper: TZipFile;
begin
  UnZipper := TZipFile.Create();
  try
    UnZipper.Open(ZipName, zmRead);
    UnZipper.ExtractAll(Destination);
    UnZipper.Close;
  finally
    FreeAndNil(UnZipper);
  end;
  Result := True;
end;

end.
